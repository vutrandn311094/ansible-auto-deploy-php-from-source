# ANSIBLE AUTO DEPLOY PHP 5.3, PHP 5.6, PHP 7.1 #

### EXECUTE COMMAND ###

ansible-playbook -i ./hosts auto_deploy_php.yml


### OUTPUT RETURN ###

```
TASK [php53 : debug] *************************************************************************************************************************************************************************
ok: [163.44.206.148] => {
    "msg": [
        "Compile PHP 5.3 success !",
        "Path: /opt/alt/php53/usr/bin/php",
        [
            "PHP 5.3.29 (cli) (built: Aug 15 2019 08:59:07) ",
            "Copyright (c) 1997-2014 The PHP Group",
            "Zend Engine v2.3.0, Copyright (c) 1998-2014 Zend Technologies"
        ]
    ]
}


TASK [php56 : debug] *************************************************************************************************************************************************************************
ok: [163.44.206.148] => {
    "msg": [
        "Compile PHP 5.6 success !",
        "Path: /opt/alt/php56/usr/bin/php",
        [
            "PHP 5.6.40 (cli) (built: Aug 15 2019 09:10:10) ",
            "Copyright (c) 1997-2016 The PHP Group",
            "Zend Engine v2.6.0, Copyright (c) 1998-2016 Zend Technologies"
        ]
    ]
}


TASK [php71 : debug] *************************************************************************************************************************************************************************
ok: [163.44.206.148] => {
    "msg": [
        "Compile PHP 7.1 success !",
        "Path: /opt/alt/php71/usr/bin/php",
        [
            "PHP 7.1.30 (cli) (built: Aug 15 2019 09:22:38) ( NTS )",
            "Copyright (c) 1997-2018 The PHP Group",
            "Zend Engine v3.1.0, Copyright (c) 1998-2018 Zend Technologies"
        ]
    ]
}

```
